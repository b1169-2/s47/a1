import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { useEffect, useState } from "react";


export default function Register(){

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [cp, setCp] = useState("")

    const [isActive, setIsActive] = useState(true)


    console.log(email)
    console.log(password)
    console.log(cp)

    function registerUser(e){
        e.preventDefault()

        setEmail("")
        setPassword("")
        setCp("")

        alert("Thank  you for registering to react booking")

    }

    useEffect( () => {
        
        if(email != "" && password != "" && cp != "" &&
            password == cp){
                setIsActive(false)
            }

    }, [email, password, cp])

    return(
        <Container className="my-5">
            <Row className="justify-content-center">
                <Col xs={10} md={8}>
                <h1>Register</h1>
                <Form 
                className="border p-3" 
                onSubmit={(e) => registerUser(e)} >
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={ (e) => {
                            setEmail(e.target.value)
                        }}
                        />
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={ (e) => {
                            setPassword(e.target.value)
                        }}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Confirm Password" 
                        value={cp}
                        onChange={(e) => {
                            setCp(e.target.value)
                        }}
                        />
                    </Form.Group>
                    <Button 
                    variant="primary" 
                    type="submit"
                    disabled={isActive}
                    >
                        Submit
                    </Button>
                </Form>
                </Col>
            </Row>
        </Container>
    )
}